/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbcbasics;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 *
 * @author Graduate
 */
public class Connect {
    private Connection itsConnection;
    private final String URL =  "jdbc:mysql://10.12.0.20/db_grad_cs_1916";
    private final String user = "dbTeam_H_04";
    private final String password = "dbT3am@3486";
    //idk the real username and password given to us 
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        Connect _main = new Connect();
         
        _main.init();
        _main.getTable("users");
        //boolean success = _main.checkLogin("alison", "gradprog2016@07");
        _main.close();
    }
 
 
    public  void    init()
    {
      try
      {
        Class.forName( "com.mysql.jdbc.Driver" );
 
        itsConnection = DriverManager.getConnection( URL, user, password );
        
      }
      catch( ClassNotFoundException e )
      {
         e.printStackTrace();
      }
      catch( SQLException e )
      {
         e.printStackTrace();
      }
    }
    
    public void close(){
        try{
            itsConnection.close();
        } catch(SQLException e){
            e.printStackTrace();
        }
        
    }
    
    public boolean checkLogin(String username, String password){
        this.init();
        Statement stmt = null;
        ResultSet rs = null;
        try{
            stmt = itsConnection.createStatement();
            rs = stmt.executeQuery("SELECT * FROM users AS U WHERE U.user_id = \"" + username + "\"");
            boolean exists = rs.next();
            if(exists){
                BufferedReader reader = new BufferedReader(new InputStreamReader(rs.getAsciiStream(2)));
                try{
                    String storedPass = reader.readLine();
                    if(storedPass.equals(password)) return true;
                    else return false;
                } catch(IOException ioe){
                    ioe.printStackTrace();
                }
            }
            else return false;
        } catch(SQLException e){
            e.printStackTrace();
        }
        this.close();
        return false;
    }
    
    public String getTable(String tableName){
        String json = "";
        String html = "";
        try
      {
        Statement stmt = null;
        ResultSet rs = null;
        stmt = itsConnection.createStatement();
        rs = stmt.executeQuery("SELECT * FROM " + tableName);
        boolean run = rs.next();
        json = "{\"columns\":{";
        html = "<table><tr>";
        for(int i = 0; i < rs.getMetaData().getColumnCount(); i++){
            String colName = rs.getMetaData().getColumnLabel(i+1);
            json += "\"col" + (i+1) + "\":\"" + colName + "\"";
            if(i < rs.getMetaData().getColumnCount()-1) json += ",";
            html += "<th>" + colName + "</th>";
        }
        html += "</tr>";
        json += "}";
        if(run) json += ",";
        
        
        while(run == true){
            html += "<tr>";
            json += "\"row" + rs.getRow() + "\":{";
            for(int i = 0; i < rs.getMetaData().getColumnCount(); i++){
                InputStream is = rs.getAsciiStream(i+1);
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                try{
                    String data = reader.readLine();
                    json += "\"col" + (i+1) + "\":\"" + data + "\"";
                    html += "<td>" + data + "</td>";
                } catch(IOException e){
                   e.printStackTrace();
                }
                if(i < rs.getMetaData().getColumnCount()-1) json += ",";
            }
            json += "}";
            run = rs.next();
            if(run) json += ",";
            html += "</tr>";
        }
        json += "}";
        html += "</table>";
        System.out.println(json);
      }
      catch( SQLException e )
      {
         e.printStackTrace();
      }
        return html;
    }
    
}

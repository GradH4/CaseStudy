/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var ajaxRequest;  // The variable that makes Ajax possible!
var target = null;
var respText = "";


//validateUserId();

// This function checks if your browser is compatiable with running ajax
function ajaxFunction()
{
   try
   {
      // Opera 8.0+, Firefox, Safari
      ajaxRequest = new XMLHttpRequest();
   }catch (e)
   {
      // Internet Explorer Browsers
      try
      {
         ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
      }catch (e) 
      {
         try
         {
            ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
         }catch (e)
         {
            // Something went wrong
            return false;
         }
      }
   }
   return true;
}

// This function is going to read the user ID from a form, send it to the server
// for verification
function validateUserId() 
{
    if( ajaxFunction() )
    {
        // Here processRequest() is the callback function.
        ajaxRequest.onreadystatechange = processRequest;

        if(target == null ) 
             target = document.getElementById("userid");
        var url = "validate.jsp?id=" + escape(target.value);

        ajaxRequest.open("GET", url, true);
        ajaxRequest.send(null);
    }
}

// This function is the callback from ajax when it receives a response back
// from the server
function processRequest() 
{
   if (ajaxRequest.readyState == 4) // request has been processed by server
   {
      if (ajaxRequest.status == 200) // server status is good
      {
         respText = ajaxRequest.responseText;
         setMessageUsingDOM( respText );
     }
      if (ajaxRequest.status == 404) // page not found
      {
         setMessageUsingDOM( "404" );
     }
   }
}

// This function is then used to update the DOM in the web page
function setMessageUsingDOM(message) 
{
    var userMessageElement = document.getElementById("userIdMessage");
    var color;
    var target = escape( document.getElementById("userid").value);
   
    if( target.length > 0 )
    {
        if (message.includes("INVALID")) 
        {
            color = "red";
        }
        else 
        {
            color = "green";
        }

        userMessageElement.style.color = color;

        $('userIdMessage').innerHTML = message;
    }
    else
        $('userIdMessage').innerHTML = "";
}


